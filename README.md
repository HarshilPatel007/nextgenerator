# Inkscape NextGenerator Extension

This is an Inkscape extension to automatically replace values (text, attribute values) in an SVG file and to then export the result to various file formats. This is useful e.g. for generating images for name badges and other similar items.

This extension is a Python rewrite of the Generator bash script extension by Aurélio A. Heckert. It is compatible with Inkscape starting from version 1.0 and requires Python 3.

**Extension dialog:**

![NextGenerator extension dialog](./example/nextGenerator_dialog.png)

**Example result:**

![Colorful badges](./example/generator-example-results.png)

## Installation

See [Inkscape FAQ](https://inkscape.org/learn/faq/#how-install-new-extensions-palettes-document-templates-symbol-sets-icon-sets-etc).

## Menu path

Extensions > Export > NextGenerator

## Additional usage hints

Some additional hints:

- check the tooltips in the extension and the Help tab for more info
- see 'example' directory for a usage example with a set of files to try things out (or online at https://gitlab.com/Moini/nextgenerator/-/tree/master/example)
- link any image that you want to use as a placeholder (don't embed)
- put the replacement images into the same directory as the placeholder, so you only need to replace the name
- sometimes, center-aligned text is easier to manage and/or looks better
- if a value in the CSV file contains commas, you must enclose the whole value in quotation marks
- the first line of your CSV file must contain the column names (header)
- you can use spaces to make your CSV file more readable
- spaces and linebreaks are not allowed in the 'Non-text values to replace' field
